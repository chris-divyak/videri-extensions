# Videri Extensions

## CGM Order Sync Extension

This extension is used to sync all of the products available in the system with the cgm_products table when a product is added, modified or deleted. This is a background extension that runs when the products are changed in CartThrob.


#### Expression Engine Hooks used

```
after_channel_entry_insert($entry, $values);
after_channel_entry_update($entry, $values, $modified);
after_channel_entry_delete($entry, $values);
```


## CGM Members Sync
This extension is used to sync all of the members available in the system with the `cgm_ibo`, `cgm_customers`, and `cgm_orders` tables when a customer buys a product on the website. This is a background extension that runs when the customer has successfully created a purchase, updated their profile, or changes their order.


#### Expression Engine Hooks used
```
member_member_login_start();
cartthrob_save_customer_info_start();
cartthrob_on_authorize();
after_member_delete($member, $values);
visitor_register_end($member_data, $member_id);
visitor_update_end();
freeform_next_submission_after_save();
```