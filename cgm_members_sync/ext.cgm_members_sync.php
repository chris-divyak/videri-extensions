<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller ee()
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cgm_members_sync_ext
{


	public $version         = CGM_MEMBERS_SYNC_VERSION;
    public $name            = CGM_MEMBERS_SYNC_NAME;
    public $description     = 'Syncing customers and IBO members on order authorization';
    public $docs_url        = 'http://www.chrisdivyak.com';
    public $settings_exist  = false;
    public $settings        = array();
    public $hooks           = array(
        'cartthrob_on_authorize',
        'after_channel_entry_update',
        'after_member_delete',
        'after_channel_entry_delete',
        'visitor_register_end',
        'after_member_field_update',
        'cartthrob_save_customer_info_start',
        'visitor_register_end',
        'after_channel_entry_insert',
        'member_member_login_start',
        'visitor_update_end',
        'freeform_next_submission_after_save',
        'member_member_logout'
    );

	// @NOTE: Extension hooks are published by UPD

	/**
	* Cartthrob_ext
	*/
	public function __construct($settings='')
	{
		$this->EE =& get_instance();
		ee()->load->dbforge();
	}


	/**
	 * Install extension
	 */
	public function activate_extension()
	{
		foreach ($this->hooks as $hook) {
             $data = array(
                'class'     =>  __CLASS__,
                'method'    =>  $hook,
                'hook'      =>  $hook,
                'settings'  =>  serialize($this->settings),
                'priority'  =>  100,
                'version'   =>  $this->version,
                'enabled'   =>  'y'
            );

            // insert in database
            ee()->db->insert('extensions', $data);
        }


        ee()->db->insert('extensions', $data);


         //----------------------------------------
        // CGM ORDERS GROUP
        //----------------------------------------
        $ibofields = array(
            'unique_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'member_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' => 999999999),
            'ibo_id_ref'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 999999999),
            'first_name'  => array('type' => 'varchar', 'constraint' => '35', 'default' => null),
            'last_name'  => array('type' => 'varchar', 'constraint' => '35',  'default' => null),
            'unique_url'  => array('type' => 'varchar', 'constraint' => '50', 'null' => TRUE),
            'token'  => array('type' =>  'varchar', 'constraint' => '75', 'null' => TRUE),
            'address'  => array('type' => 'varchar', 'constraint' => '55','null' => TRUE),
            'city'  => array('type' => 'varchar', 'constraint' => '35','null' => TRUE),
            'state'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'zip'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'terms'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'group_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' =>999999999),
            'create_dtm'  => array('type' => 'datetime'),
            'update_dtm'  => array('type' => 'datetime'),
        );

        ee()->dbforge->add_field($ibofields);
        ee()->dbforge->add_key('unique_id', TRUE);
        ee()->dbforge->create_table('cgm_ibo', TRUE);

         $customerfields = array(
            'unique_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'member_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' => 999999999),
            'ibo_id_ref'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 999999999),
            'first_name'  => array('type' => 'varchar', 'constraint' => '35', 'default' => null),
            'last_name'  => array('type' => 'varchar', 'constraint' => '35',  'default' => null),
            'unique_url'  => array('type' => 'varchar', 'constraint' => '50', 'null' => TRUE),
            'token'  => array('type' =>  'varchar', 'constraint' => '75', 'null' => TRUE),
            'address'  => array('type' => 'varchar', 'constraint' => '55','null' => TRUE),
            'city'  => array('type' => 'varchar', 'constraint' => '35','null' => TRUE),
            'state'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'zip'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'terms'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'group_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' =>999999999),
            'create_dtm'  => array('type' => 'datetime'),
            'update_dtm'  => array('type' => 'datetime'),
        );


        ee()->dbforge->add_field($customerfields);
        ee()->dbforge->add_key('unique_id', TRUE);
        ee()->dbforge->create_table('cgm_customers', TRUE);

        $productfields = array(
            'customer_product_sale_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'customer_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' => 999999999),
            'ibo_id_ref'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 999999999),
            'order_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 0),
            'product_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 0),
            'prod_unit_qty' => array('type' => '	mediumint', 'constraint' => '6'),
            'prod_sale_dtm'  => array('type' => 'datetime'),
            'create_dtm'  => array('type' => 'datetime'),
            'rpm_date' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '11', 'default' => 0),
            'prod_sale_total_amt' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '8', 'default' =>0),
            'prod_sale_total_tax_amt' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '8', 'default' =>0),
            'customer_address'  => array('type' => 'varchar', 'constraint' => '55','null' => TRUE),
            'customer_city'  => array('type' => 'varchar', 'constraint' => '35','null' => TRUE),
            'customer_state'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'customer_zip'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
        );


        ee()->dbforge->add_field($productfields);
        ee()->dbforge->add_key('customer_product_sale_id', TRUE);
        ee()->dbforge->create_table('cgm_customer_orders', TRUE);

         $downpaymentfields = array(
            'customer_downpayment_sale_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'customer_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' => 999999999),
            'ibo_id_ref'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 999999999),
            'order_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 0),
            'product_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 0),
            'prod_unit_qty' => array('type' => '	mediumint', 'constraint' => '6'),
            'prod_sale_dtm'  => array('type' => 'datetime'),
            'create_dtm'  => array('type' => 'datetime'),
            'rpm_date' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '11', 'default' => 0),
            'prod_sale_total_amt' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '8', 'default' =>0),
            'prod_sale_total_tax_amt' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '8', 'default' =>0),
            'customer_address'  => array('type' => 'varchar', 'constraint' => '55','null' => TRUE),
            'customer_city'  => array('type' => 'varchar', 'constraint' => '35','null' => TRUE),
            'customer_state'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'customer_zip'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
        );


        ee()->dbforge->add_field($downpaymentfields);
        ee()->dbforge->add_key('customer_downpayment_sale_id', TRUE);
        ee()->dbforge->create_table('cgm_customer_downpayments', TRUE);

        $iboProductfields = array(
            'ibo_product_sale_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'customer_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5', 'default' => 999999999),
            'ibo_id_ref'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 999999999),
            'order_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 0),
            'product_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '5','default' => 0),
            'prod_unit_qty' =>array('type' => '	mediumint', 'constraint' => '6'),
            'prod_sale_dtm'  => array('type' => 'datetime'),
            'create_dtm'  => array('type' => 'datetime'),
            'rpm_date' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '11', 'default' => 0),
            'prod_sale_total_amt' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '8', 'default' =>0),
            'prod_sale_total_tax_amt' => array('type' => 'INT', 'unsigned' => TRUE, 'constraint' => '8', 'default' =>0),
            'customer_address'  => array('type' => 'varchar', 'constraint' => '55','null' => TRUE),
            'customer_city'  => array('type' => 'varchar', 'constraint' => '35','null' => TRUE),
            'customer_state'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
            'customer_zip'  => array('type' => 'varchar', 'constraint' => '5','null' => TRUE),
        );


        ee()->dbforge->add_field($iboProductfields);
        ee()->dbforge->add_key('ibo_product_sale_id', TRUE);
        ee()->dbforge->create_table('cgm_ibo_orders', TRUE);




    }


	/**
	 * Uninstall extension
	 */
	public function disable_extension()
	{
		//ee()->dbforge->drop_table('cgm_ibo');
		//ee()->dbforge->drop_table('cgm_customers');
		ee()->db->where('class', __CLASS__);
        ee()->db->delete('extensions');

	}

	/**
	 * Update extension
	 */
	public function update_extension($current = '')
	{
		if ($current == $this->version) return false;

        // Get all existing ones
        $dbexts = array();
        $query = ee()->db->select('*')->from('extensions')->where('class', __CLASS__)->get();

        foreach ($query->result() as $row) {
            $dbexts[$row->hook] = $row;
        }

        // Add the new ones
        foreach ($this->hooks as $hook) {
            if (isset($dbexts[$hook]) === true) continue;

            $data = array(
                'class'     =>  __CLASS__,
                'method'    =>  $hook,
                'hook'      =>  $hook,
                'settings'  =>  serialize($this->settings),
                'priority'  =>  100,
                'version'   =>  $this->version,
                'enabled'   =>  'y'
            );

            // insert in database
            ee()->db->insert('extensions', $data);
        }

        // Delete old ones
        foreach ($dbexts as $hook => $ext) {
            if (in_array($hook, $this->hooks) === true) continue;

            ee()->db->where('hook', $hook);
            ee()->db->where('class', __CLASS__);
            ee()->db->delete('extensions');
        }

        // Update the version number for all remaining hooks
        ee()->db->where('class', __CLASS__)->update('extensions', array('version' => $this->version));
	}





	public function cartthrob_on_authorize()
	{

		//----------------------------------------------//
 		//--------- INSERT EACH MEMBER INFO  ----------//
 		//---------------------------------------------//
		//get member DATA
		$member_id  = ee()->session->userdata('member_id');
		$group_id  = ee()->session->userdata('group_id');

		//get member data from visitors channel data
		$memberDemoData = ee()->db->select('*')->from('exp_member_data')->where(array('member_id' => $member_id))->get();
		$getEmail = ee()->db->select('*')->from('exp_members')->where(array('member_id' => $member_id))->get();

		//get email of user
		if ($getEmail->num_rows() > 0){
		    foreach($getEmail->result_array() as $row)
		    {
			    $email = $row['username'];
			}
		}

		//get all member data for user
		if ($memberDemoData->num_rows() > 0){
		    foreach($memberDemoData->result_array() as $row)
		    {
			    //set member info
		        $first_name  = $row['m_field_id_17'];
		        $last_name  = $row['m_field_id_18'];
		        $address  = $row['m_field_id_8'];
		        $city  = $row['m_field_id_10'];
		        $state  = $row['m_field_id_11'];
		        $zip  = $row['m_field_id_12'];
		        $ibo_id_ref  = $row['m_field_id_14'];
		        $terms  = $row['m_field_id_16'];
		        $unique_url = $row['m_field_id_7'];
		        $token = $row['m_field_id_5'];

		    }
		}

		//set current date
		$date = date("Y-m-d H:i:s");


		//set full data for insert
		 $data = array(
	        'member_id'  => $member_id,
	        'first_name'  => $first_name,
	        'last_name'  => $last_name,
	        'email' => $email,
	        'unique_url'  => $unique_url,
	        'token '  => $token ,
	        'address'  => $address,
	        'city'  => $city,
	        'state'  => $state,
	        'zip'  => $zip,
	        'ibo_id_ref' => $ibo_id_ref,
	        'terms' => $terms,
	        'group_id' => $group_id,
	        'create_dtm' => $date,
		);
		//set parent ibo data to update channel entries
		$memberData = array(
	        'field_id_112'  => $ibo_id_ref,
	        'field_id_146' => $group_id,
	    );

	    //update payment as yes for customer
		$memberIboData = array(
	        'm_field_id_22'  => 'y'
	    );

	    ee()->db->where(array('member_id' => $member_id));
		ee()->db->update('exp_member_data', $memberIboData);


		//check if members already exist
		$checkIboData = ee()->db->select('member_id')->from('exp_cgm_ibo')->where(array('member_id' => $member_id))->get();

		$checkCustomerData = ee()->db->select('member_id')->from('exp_cgm_customers')->where(array('member_id' => $member_id))->get();

		//if IBO
		if($group_id == 6 || $group_id == '6'){
			//check if member already exists
			if($checkIboData->num_rows() == 0){
				ee()->db->insert('cgm_ibo', $data);

			}//if exists, update member profile
			else{
				//update cgm ibo member data
				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('cgm_ibo', $data);
			}

		}

		//if customer and not IBO
		if($group_id == 8 || $group_id == '8'){
			//check if member already exists, if not add
			if($checkCustomerData->num_rows() == 0){

				//add customer to CGM table
				ee()->db->insert('cgm_customers', $data);

			}
			//if exists, update member profile
			else{
				//update customer in CGM table
				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('cgm_customers', $data);
			}
 		}

 		//----------------------------------------------//
 		//--------- INSERT EACH CART CONTENTS ----------//
 		//---------------------------------------------//

 		$order_id = ee()->cartthrob->cart->order('order_id');
 		$orders = ee()->db->select('entry_id, quantity, entry_date, price, price_plus_tax')->from('exp_cartthrob_order_items')->where(array('order_id' => $order_id))->get();


 		$memberDataUpdate = array(
	        'field_id_112'  => $ibo_id_ref,
	        'field_id_146' => 6,
	    );
	    $groupUpdate = array(
	        'group_id' => 6,
	    );
	     $groupUpdateCustomer = array(
	        'group_id' => 8,
	    );

 		 if ($orders->num_rows() > 0){
		    foreach($orders->result_array() as $row)
		    {


				$product_id = $row['entry_id'];
				$prod_unit_qty = $row['quantity'];
				$rpm_date = $row['entry_date'];
				$prod_sale_total_amt = $row['price'];
				$prod_sale_total_tax_amt = $row['price_plus_tax'];
				$checkIboOrderData = ee()->db->select('customer_id')->from('exp_cgm_ibo_orders')->where(array('customer_id' => $member_id))->get();
				if($group_id ==='6' && $product_id != 13){

					//check if actually an IBO paid order
					if($checkIboOrderData->num_rows() != 0){
						$ibo_id_ref = $member_id;
					}
					//if not IBO paid but group id=6, remove from cgm_ibo and add to cgm_customer and update group_id=8
					else{
						//update member data
			 		 	ee()->db->where(array('member_id' =>  $member_id));
			 		 	ee()->db->update('exp_members',  $groupUpdateCustomer);

			 		 	$group_id = '8';
			 		 	//if customer, remove from customer table
			 		 	if($checkIboData->num_rows() != 0){
				 			ee()->db->delete('exp_cgm_ibo',array('member_id' => $member_id));
				 		}

			 		 	//add customer to new ibo table
			 		 	if($checkCustomerData->num_rows() == 0){
			 		 		ee()->db->insert('exp_cgm_customers',$data);
			 		 	}
					}
				}

				$orderData = array(
					'customer_id' => $member_id,
					'ibo_id_ref' => $ibo_id_ref,
					'order_id' => $order_id,
					'product_id' => $product_id,
					'prod_unit_qty' =>$prod_unit_qty,
					'prod_sale_dtm' => $date,
					'create_dtm' => $date,
					'rpm_date' => $rpm_date,
					'prod_sale_total_amt' => $prod_sale_total_amt,
					'prod_sale_total_tax_amt' => $prod_sale_total_tax_amt,
					'customer_address' => $address,
					'customer_city'  => $city,
					'customer_state' => $state,
					'customer_zip' => $zip,
				);
				//if ibo payment
				if($product_id == 13){

					//check if members already exist
					$checkIboData = ee()->db->select('member_id')->from('exp_cgm_ibo')->where(array('member_id' => $member_id))->get();
					$checkCustomerData = ee()->db->select('member_id')->from('exp_cgm_customers')->where(array('member_id' => $member_id))->get();
		 		 	ee()->db->insert('cgm_ibo_orders', $orderData);

		 		 	//update member data
		 		 	ee()->db->where(array('member_id' =>  $member_id));
		 		 	ee()->db->update('exp_members', $groupUpdate);


		 		 	//if customer, remove from customer table
		 		 	if($checkCustomerData->num_rows() != 0){
			 			ee()->db->delete('exp_cgm_customers',array('member_id' => $member_id));
			 		}

		 		 	//add customer to new ibo table
		 		 	if($checkIboData->num_rows() == 0){
		 		 		ee()->db->insert('exp_cgm_ibo',$data);
		 		 	}

		 		 }

		 		 //if not ibo or beef payments
		 		 if($product_id == 1003 || $product_id ==1090 || $product_id ==1091 || $product_id ==1092 || $product_id ==1093 || $product_id ==1094 || $product_id ==1095 || $product_id ==1096){
			 		ee()->db->insert('cgm_customer_downpayments', $orderData);
			 	}
		 		 //if not ibo or down payments
		 		 if($product_id != 13 && $product_id != 1003 && $product_id !=1090 && $product_id !=1091 && $product_id !=1092 && $product_id !=1093 && $product_id !=1094 && $product_id !=1095 && $product_id !=1096){
			 		ee()->db->insert('cgm_customer_orders', $orderData);
			 	}
		    }
		}


 		return true;
	}



	public function after_member_delete($member, $values){

		$member_id  = $member->member_id;

		$checkIboData = ee()->db->select('member_id')->from('exp_cgm_ibo')->where(array('member_id' => $member_id))->get();
		$checkCustomerData = ee()->db->select('member_id')->from('exp_cgm_customers')->where(array('member_id' => $member_id))->get();

		if($checkCustomerData->num_rows() != 0){
			ee()->db->delete('exp_cgm_customers',array('member_id' => $member_id));
		}

		if($checkIboData->num_rows() != 0){
			ee()->db->delete('exp_cgm_ibo',array('member_id' => $member_id));
		}


	}



	public function visitor_register_end($member_data, $member_id){


		$member_email = $member_data->username;
		$token = $member_data['member_token'];
		$firstName = $member_data['member_first_name'];
		$lastName = $member_data['member_last_name'];


		//Function: generate uniqueUrl

		$generateUniqueUrl = ee()->db->query('select fx_exp_return_uniqueCode('.$member_id.',"'.$firstName.'","'.$lastName.'")');
		$array = json_encode($generateUniqueUrl->result_array());
		$result = json_decode($array);
		$uniqueUrl = reset($result[0]);




		//pass to member_data table
		$memberData =  array(
			'm_field_id_7' => $uniqueUrl,
			'm_field_id_17' => $firstName,
			'm_field_id_18' => $lastName
		);

		//pass to channel_data table
		$channelData =  array(
			'field_id_105' => $uniqueUrl
		);
		//pass to cgm_ibo/cgm_customer tables
		$cgmData =  array(
			'unique_url' => $uniqueUrl,
			'first_name' => $firstName,
			'last_name' => $lastName
		);

		$checkMemberData = ee()->db->select('m_field_id_7')->from('exp_member_data')->where(array('member_id' => $member_id))->get();
		$checkCustomerData = ee()->db->select('member_id')->from('exp_cgm_customers')->where(array('member_id' => $member_id))->get();
		$checkIboData = ee()->db->select('member_id')->from('exp_cgm_ibo')->where(array('member_id' => $member_id))->get();

			//update all tables with unique URL data;
			if($checkMemberData->num_rows() != 0){
				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('exp_member_data', $memberData);
			}

			if($checkIboData->num_rows() != 0){
				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('cgm_customers', $cgmData);
			}

			if($checkMemberData->num_rows() != 0){
				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('cgm_customers', $cgmData);
			}


	}

	public function visitor_update_end(){

		//get member DATA
		$member_id  = ee()->session->userdata('member_id');

		$firstName = $_POST['member_first_name'];
		$lastName = $_POST['member_last_name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$address2 = $_POST['address2'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zip = $_POST['zip'];
		$company = $_POST['company'];

		$newAddress = $address.' '.$address2;
		$name = $firstName.' '.$lastName;


		//pass to member_data table
		$memberData =  array(
			'm_field_id_17' => $firstName,
			'm_field_id_18' => $lastName,
			'm_field_id_15' => $company,
			'm_field_id_8' => $address,
			'm_field_id_9' => $address2,
			'm_field_id_10' => $city,
			'm_field_id_11' => $state,
			'm_field_id_12' => $zip,
			'm_field_id_13' => $phone
		);

		$members =  array(
			'username' => $email,
			'email' => $email,
			'screen_name' => $name,
		);


		//pass to cgm_ibo/cgm_customer tables
		$cgmData =  array(
			'first_name' => $firstName,
			'last_name' => $lastName,
			'email' => $email,
			'address' => $newAddress,
			'city' => $city,
			'state' => $state,
			'zip' => $zip
		);


				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('exp_member_data', $memberData);

				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('exp_members', $members);

				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('cgm_customers', $cgmData);

				ee()->db->where(array('member_id' => $member_id));
				ee()->db->update('cgm_ibo', $cgmData);

	}

	public function member_member_login_start(){
		$member_group = ee()->session->userdata('member_group');

		if($member_group == '6'){
			header("Location: /dashboard");
		}else{
			header("Location: /store/account");
		}
	}
	//--------TESTED WORKING------------
	//update member data before payment information
	public function cartthrob_save_customer_info_start(){

		$member_id = ee()->session->userdata('member_id');

		$firstName = $_POST['first_name'];
		$lastName = $_POST['last_name'];
		$address = $_POST['address'];
		$address2 = $_POST['address2'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zip = $_POST['zip'];
		$company = $_POST['company_name'];
		$phone = $_POST['phone'];
		$ibo_ref_id = $_POST['ibo_ref_id'];

		//define ibo_partner by input from customer channel
		$checkForIboPartner = ee()->db->select('*')->from('exp_member_data')->where(array('member_id' => $member_id))->get();

		//if no IBO already set, get post or set for user
		if ($checkForIboPartner->num_rows() > 0){
		    foreach($checkForIboPartner->result_array() as $row)
		    {
				if($row['m_field_id_14'] === null || $row['m_field_id_14'] === ''){


					//if no partner ibo, default
					if($ibo_ref_id=== null || empty($ibo_ref_id) || $ibo_ref_id === $member_id){
						$ibo_ref_id = 999999999;
					}

					$getIboName = ee()->db->select('*')->from('exp_member_data')->where(array('member_id' => $ibo_ref_id))->get();
					if ($getIboName->num_rows() > 0){
					    foreach($getIboName->result_array() as $row)
					    {
						    $iboName = $row['m_field_id_19'];
						}
			    	}

					$customer = array(
						'm_field_id_17'=>$firstName,
						'm_field_id_18'=>$lastName,
						'm_field_id_8'=>$address,
						'm_field_id_9'=>$address2,
						'm_field_id_10'=>$city,
						'm_field_id_11'=>$state,
						'm_field_id_12'=>$zip,
						'm_field_id_15'=>$company,
						'm_field_id_13'=>$phone,
						'm_field_id_14'=>$ibo_ref_id,
						'm_field_id_19'=>$iboName
					);

				}else{
					if($row['m_field_id_14'] !== $ibo_ref_id){
						$ibo_ref_id = $_POST['ibo_ref_id'];
					}else{
						$ibo_ref_id = $row['m_field_id_14'];
					}
					$customer = array(
						'm_field_id_17'=>$firstName,
						'm_field_id_18'=>$lastName,
						'm_field_id_8'=>$address,
						'm_field_id_9'=>$address2,
						'm_field_id_10'=>$city,
						'm_field_id_11'=>$state,
						'm_field_id_12'=>$zip,
						'm_field_id_15'=>$company,
						'm_field_id_13'=>$phone,
						'm_field_id_14'=>$ibo_ref_id
					);
				}
				//update member data in channel data
				ee()->db->where(array('member_id' =>  $member_id));
				ee()->db->update('exp_member_data', $customer);
				return false;
			}
		}


	}

	public function freeform_next_submission_after_save(){
		$member_id = ee()->session->userdata('member_id');

		$data =  array(
			'm_field_id_23' => 'p',
		);

		//update member data in channel data
		ee()->db->where(array('member_id' =>  $member_id));
		ee()->db->update('exp_member_data', $data);
	}

	public function member_member_logout(){

		$_SESSION = array();
	}



}
// END CLASS
