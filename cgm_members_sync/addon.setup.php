<?php


if ( ! defined('CGM_MEMBERS_SYNC_VERSION'))
{
	define('CGM_MEMBERS_SYNC_VERSION', '1.0.0');
    define('CGM_MEMBERS_SYNC_NAME',         'CGM Members Sync');
    define('CGM_MEMBERS_SYNC_CLASS_NAME',   'cgm_members_sync');
}
$config['name'] = CGM_MEMBERS_SYNC_NAME;
$config['version'] =  CGM_MEMBERS_SYNC_VERSION;

return array(
    'author'      => 'Chris Divyak',
    'author_url'  => 'https://www.chrisdivyak.com',
    'name'        => CGM_MEMBERS_SYNC_NAME,
    'description' => 'CGM Order sync for CartThrob Products',
    'version'     => CGM_MEMBERS_SYNC_VERSION,
    'namespace'   => 'cgm_members_sync\cgm_members_sync',
    'settings_exist' => FALSE,
);
