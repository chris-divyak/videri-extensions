<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller ee()
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cgm_order_sync_ext
{


	public $version         = CGM_ORDER_SYNC_VERSION;
    public $name            = CGM_ORDER_SYNC_NAME;
    public $description     = 'Supports the Orders Module in various functions.';
    public $docs_url        = 'http://www.chrisdivyak.com';
    public $settings_exist  = false;
    public $settings        = array();
    public $hooks           = array(
        'after_channel_entry_insert',
        'after_channel_entry_update',
        'after_channel_entry_delete',
    );
    public $table			= 'cgm_products';

	// @NOTE: Extension hooks are published by UPD

	/**
	* Cartthrob_ext
	*/
	public function __construct($settings='')
	{
		$this->EE =& get_instance();
		ee()->load->dbforge();
	}


	/**
	 * Install extension
	 */
	public function activate_extension()
	{
		foreach ($this->hooks as $hook) {
             $data = array(
                'class'     =>  __CLASS__,
                'method'    =>  $hook,
                'hook'      =>  $hook,
                'settings'  =>  serialize($this->settings),
                'priority'  =>  100,
                'version'   =>  $this->version,
                'enabled'   =>  'y'
            );

            // insert in database
            ee()->db->insert('extensions', $data);
        }


        ee()->db->insert('extensions', $data);


         //----------------------------------------
        // CGM ORDERS GROUP
        //----------------------------------------
        $fields = array(
            'unique_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'channel_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'default' => 0),
            'product_id'  => array('type' => 'INT', 'unsigned' => TRUE, 'default' => 0),
            'title'  => array('type' => 'varchar', 'constraint' => '100',  'default' => null),
            'product_price'  => array('type' => 'varchar', 'constraint' => '25', 'null' => TRUE),
            'weight'  => array('type' =>  'varchar', 'constraint' => '10', 'null' => TRUE),
            'inventory'  => array('type' => 'varchar', 'constraint' => '100','null' => TRUE),
            'sku'  => array('type' => 'varchar', 'constraint' => '100','null' => TRUE),

        );

        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('unique_id', TRUE);
        ee()->dbforge->create_table('cgm_products', TRUE);
    }
	/**
	 * Uninstall extension
	 */
	public function disable_extension()
	{
		//ee()->dbforge->drop_table('cgm_products');
		ee()->db->where('class', __CLASS__);
        ee()->db->delete('extensions');

	}

	/**
	 * Update extension
	 */
	public function update_extension($current = '')
	{
		if ($current == $this->version) return false;

        // Get all existing ones
        $dbexts = array();
        $query = ee()->db->select('*')->from('extensions')->where('class', __CLASS__)->get();

        foreach ($query->result() as $row) {
            $dbexts[$row->hook] = $row;
        }

        // Add the new ones
        foreach ($this->hooks as $hook) {
            if (isset($dbexts[$hook]) === true) continue;

            $data = array(
                'class'     =>  __CLASS__,
                'method'    =>  $hook,
                'hook'      =>  $hook,
                'settings'  =>  serialize($this->settings),
                'priority'  =>  100,
                'version'   =>  $this->version,
                'enabled'   =>  'y'
            );

            // insert in database
            ee()->db->insert('extensions', $data);
        }

        // Delete old ones
        foreach ($dbexts as $hook => $ext) {
            if (in_array($hook, $this->hooks) === true) continue;

            ee()->db->where('hook', $hook);
            ee()->db->where('class', __CLASS__);
            ee()->db->delete('extensions');
        }

        // Update the version number for all remaining hooks
        ee()->db->where('class', __CLASS__)->update('extensions', array('version' => $this->version));
	}




	public function after_channel_entry_insert($entry, $values)
	{
		$channel_id = $entry->channel_id;

		//if a product, insert
		if($channel_id === '5'){

			$product_id= $entry->entry_id;
			$title = $entry->title;
			$product_price = $entry->field_id_141;
			$weight = $entry->field_id_19;
			$inventory = $entry->field_id_20;
			$sku = $entry->field_id_21;

			$data = array(
	            'channel_id'  => $channel_id,
	            'product_id'  => $product_id,
	            'title'  => $title,
	            'product_price'  => $product_price,
	            'weight'  => $weight,
	            'inventory'  => $inventory,
	            'sku'  => $sku,
			);

			//update cgm_products table
			ee()->db->insert('cgm_products', $data);
		}

	}
	public function after_channel_entry_update($entry, $values, $modified)
	{

		$channel_id = $entry->channel_id;

		$product_id= $entry->entry_id;
		$title = $entry->title;


		//if a product, insert
		if($channel_id === '5'){

			$product_price = $entry->field_id_141;
			$weight = $entry->field_id_19;
			$inventory = $entry->field_id_20;
			$sku = $entry->field_id_21;

			$data = array(
	            'channel_id'  => $channel_id,
	            'product_id'  => $product_id,
	            'title'  => $title,
	            'product_price'  => $product_price,
	            'weight'  => $weight,
	            'inventory'  => $inventory,
	            'sku'  => $sku,
			);

			//update cgm_products table
			$productData = ee()->db->select('*')->from('cgm_products')->where(array('product_id' => $product_id))->get();
			if ($productData->num_rows() > 0){
		    	foreach($productData->result_array() as $row)
				{
					ee()->db->update('cgm_products', $data, array('product_id' => $product_id));
				}
			}else{
				ee()->db->insert('cgm_products', $data);
			}

		}

		//if customer order update
		if($channel_id === '6'){

			$product_price = $entry->field_id_34;
			$weight = $entry->field_id_141;
			//shipping address
			$address = $entry->field_id_46;
			$zip = substr($address, -5);
			$data = array(
	            'prod_sale_total_amt' => $product_price,
	            'prod_sale_total_tax_amt' => $product_price,
	            'customer_address' => $address,
	            'customer_zip' => $zip
			);

			$iboData = ee()->db->select('*')->from('cgm_ibo_orders')->where(array('order_id' => $product_id))->get();
			$customerData = ee()->db->select('*')->from('cgm_customer_orders')->where(array('order_id' => $product_id))->get();
			if ($iboData->num_rows() > 0){
		    	foreach($iboData->result_array() as $row)
				{

				ee()->db->update('cgm_ibo_orders', $data, array('order_id' => $product_id));
				}
			}

			if ($customerData->num_rows() > 0){
		    	foreach($customerData->result_array() as $row)
				{

				ee()->db->update('cgm_customer_orders', $data, array('order_id' => $product_id));
				}
			}

		}


	}
	public function after_channel_entry_delete($entry, $values)
	{
		$channel_id = $entry->channel_id;
		$product_id= $entry->entry_id;
		ee()->db->delete('cgm_products',array('product_id' => $product_id));
		if($channel_id === '6'){
			ee()->db->delete('cgm_ibo_orders', array('order_id' => $product_id));
			ee()->db->delete('cgm_customer_orders', array('order_id' => $product_id));
		}

	}



}
// END CLASS
