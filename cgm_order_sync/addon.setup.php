<?php


if ( ! defined('CGM_ORDER_SYNC_VERSION'))
{
	define('CGM_ORDER_SYNC_VERSION', '1.0.0');
    define('CGM_ORDER_SYNC_NAME',         'CGM Order Sync');
    define('CGM_ORDER_SYNC_CLASS_NAME',   'cgm_order_sync');
}
$config['name'] = CGM_ORDER_SYNC_NAME;
$config['version'] =  CGM_ORDER_SYNC_VERSION;

return array(
    'author'      => 'Chris Divyak',
    'author_url'  => 'https://www.chrisdivyak.com',
    'name'        => CGM_ORDER_SYNC_NAME,
    'description' => 'CGM Order sync for CartThrob Products',
    'version'     => CGM_ORDER_SYNC_VERSION,
    'namespace'   => 'cgm_sync_orders\cgm_sync_orders',
    'settings_exist' => FALSE,
);
